import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.20 as Kirigami

Kirigami.ApplicationWindow {
    id: root
    width: 600
    height: 500
    title: "Kirigami Test Windows"

    globalDrawer: Kirigami.GlobalDrawer {
        isMenu: false
        title: "Global drawer with an icon"
        titleIcon: "applications-system"
        actions: [
            Kirigami.Action {
                text: "Action with an icon"
                icon.name: "applications-graphics"
            }
        ]
    }

    pageStack.initialPage: Kirigami.Page {
        title: "Kirigami Test"

        ColumnLayout {
            anchors.fill: parent

            Kirigami.InlineMessage {
                Layout.alignment: Qt.AlignTop
                Layout.fillWidth: true
                text: "InlineMessage with an icon"
                icon.source: "actor"
                visible: true
            }

            Controls.Button {
                Layout.alignment: Qt.AlignHCenter
                icon.name: "kde"
                text: "A Small KDE Icon"
                onClicked: showPassiveNotification("KDE Button Clicked!")
            }
        }
    }
}
