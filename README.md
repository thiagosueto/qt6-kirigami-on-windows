# Qt6 Kirigami on Windows

This repo is an attempt to run Kirigami apps on Windows, built with Craft.

Currently, I want to make Kirigami apps show Breeze icons while on Windows. First Qt6, but also Qt5. Trying Qt6 first.

# Building

* Install [Craft on Windows](https://community.kde.org/Get_Involved/development/Windows) with MinGW
* Run `craft kirigami kcoreaddons ki18n kiconthemes breeze qqc2-desktop-style`
* Change directories to this project
* `cmake -B build/ -DBUILD_WITH_QT6=ON -G "MinGW Makefiles"`
* `cmake --build build/`
* `cmake --install build/`
* The app will be installed to C:\CraftRoot\bin and available in the Craft env path
* `helloworld.exe`

# Result

* Running the executable from the Craft env: icons do not show up
* Running the executable from Explorer: icons do not show up
* Setting `$Env:QT_QUICK_CONTROLS_STYLE = "org.kde.desktop"` and then running the executable from the Craft env: icons show up

# Solution?

Just force desktop style:

```c++
QQuickStyle::setStyle(QStringLiteral("org.kde.desktop"));
```
